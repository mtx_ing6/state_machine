/*
 * statetablepattern.h
 *
 *  Created on: 03/10/2019
 *      Author: pc
 */

#ifndef STATETABLEPATTERN_H_
#define STATETABLEPATTERN_H_

typedef enum TSTATETYPE {
	NULL_STATE,
	NONUMBER_STATE,
	GN_PROCESSINGWHOLEPART_STATE,
	GN_PROCESSINGFRACTIONALPART_STATE
} TSTATETYPE;

typedef void(*ActionPtr0)(void*);

typedef void(*ActionPtr1)(void*, char);

typedef enum EventType {
	EVDIGIT,
	EVDOT,
	EVWHITESPACE,
	EVENDOFSTRING
} EventType;

typedef struct {
	EventType eType;
	union eventData {
		char c;
	} ed;
} Event;

typedef struct ap {
	int nParams; /* 0 or 1 in this case */
	union {
		ActionPtr0 a0;
		ActionPtr1 a1;
	} aPtr;
} ActionType;

typedef ActionType* ActionPtr;

/* This function returns an int that is treated as a Boolean (0 == FALSE) and, if present, is
used to determine whether or not the transition is taken. Note that this function takes no
parameters (other than the object me pointer). */
typedef int (*GuardType)(void *);

typedef struct TableEntryType {
	ActionPtr entryActionPtr;
	ActionPtr exitActionPtr;
	GuardType guardPtr;
	TSTATETYPE newState;
	ActionPtr transActionPtr;
} TableEntryType;

/* digit returns an int value of the char
That is: return c-'0' */
int digit(char c);

#endif /* STATETABLEPATTERN_H_ */
